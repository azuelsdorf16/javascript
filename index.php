<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <?php include('/var/www/html/bootstrapInclude.php'); ?>
</head>
<body>
<script src = "text/javascript">
//Detects whether the user has Caps Lock on.
document.onkeypress = function(e) { 
    e = e || window.event;
    var s = String.fromCharCode(e.keyCode || e.which);
    if ((s.toUpperCase() === s && !e.shiftKey) || (e.shiftKey && s.toLowerCase() === s)) {
        alert("Caps lock is on!");
    }
};
</script>
<div class="container">
  <div class="jumbotron">
    <h1>My First Bootstrap Page</h1>
    <p>Resize this responsive page to see the effect!</p> 
  </div>
  <div class="row">
    <div class="col-sm-4">
      <h3>Column 1</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
      <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris...</p>
    </div>
    <div class="col-sm-4">
      <h3>Column 2</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
      <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris...</p>
    </div>
    <div class="col-sm-4">
      <h3>Column 3</h3>        
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
      <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris...</p>
    </div>
  </div>
</div>

</body>
</html>
