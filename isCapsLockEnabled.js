/*
 * Programmer: Andrew Zuelsdorf
 * Date: 23 February 2015
 * This program makes an alert message pop
 * up if the user types a character and caps
 * lock is enabled.
 */

function isAlpha(character) {
    if (character >= String.fromCharCode(0x41) &&
        character <= String.fromCharCode(0x5A)) {
        return true;
    }
    else if (character >= String.fromCharCode(0x61) &&
        character <= String.fromCharCode(0x7A)) {
        return true;
    }
    else if (character >= String.fromCharCode(0xC0) &&
             character <= String.fromCharCode(0x2B8)) {
        return true;
    }
    else if (character >= String.fromCharCode(0x363) &&
             character <= String.fromCharCode(0x1FFF)) {
        return true;
    }
    else {
        return false;
    }
}

document.onkeypress = function(e) { 
    e = e || window.event;
    var s = String.fromCharCode(e.keyCode || e.which);
    if ((s.toUpperCase() === s && !e.shiftKey) || (e.shiftKey && s.toLowerCase() === s)) {
        if (isAlpha(s)) {
            alert("Caps lock is on!");
        }
    }
};
