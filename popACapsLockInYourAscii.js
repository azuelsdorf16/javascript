document.onkeypress = function(e) { 
    e = e || window.event;
    var s = String.fromCharCode(e.keyCode || e.which);
    if ((s.toUpperCase() === s && !e.shiftKey) || (e.shiftKey && s.toLowerCase() === s)) {
        var t = s.toLowerCase();
        if (t >= 'a' && t <= 'z') {
            alert("Caps lock is on!");
        }
    }
};
