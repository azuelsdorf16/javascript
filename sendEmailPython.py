#! /usr/bin/env python

#Contains the actual sending functions and objects
import smtplib

#Contains the email modules we will need.
from email.mime.text import MIMEText

#A message consisting ONLY of ASCII characters.

def sendAnEmail(toAddrs, fromAddrs, messages, subjects):
    msgNumber = 0
    assert(len(toAddrs) == len(fromAddrs) and len(messages) == len(subjects) and len(subjects) == len(fromAddrs))

    for msgNumber in range(len(toAddrs)):
        emailMsg = MIMEText(messages[msgNumber])

        emailMsg['Subject'] = messages[msgNumber]
        emailMsg['From'] = fromAddrs[msgNumber]
        emailMsg['To'] = toAddrs[msgNumber]

        s = smtplib.SMTP("localhost")
        s.sendmail(fromAddrs[msgNumber], [toAddrs[msgNumber]], emailMsg.as_string())
    s.quit()

def main():

    message = """Hi member of Team 5,
This is Andrew. I am doing some rapid prototyping
of a mail server on Ubuntu Linux. I would appreciate
it if you would send me an email saying that you received
this email. My email address is andrezu1@umbc.edu

--Andrew"""


    toAddrs = ["andrezu1@umbc.edu", "aionda1@umbc.edu", "nacline1@umbc.edu", "pdella1@umbc.edu", "jonbrew1@umbc.edu"]
    fromAddrs = ["andrezu1@umbc.edu" for i in range(5)]

    messages = [message for i in range(5)]
    subjects = ["This is Andrew Zuelsdorf's Virtual Box" for i in range(5)]
    print (messages)
    print (subjects)

    sendAnEmail(toAddrs, fromAddrs, messages, subjects)

if __name__ == "__main__":
    main()
